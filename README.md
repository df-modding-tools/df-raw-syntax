# DF Raw Syntax

This project describes the Syntax of the Dwarf Fortress Raw files, also known as mod files.

The syntax is written to reduce ambiguity and define a standard.
This standard can in turn be used to create tools like syntax highlighters and/or language servers.

## Syntax

The current syntax is for DF version `v0.47.04`.

- **[View Syntax](syntax/start.md)**
- **[Naming Conventions](syntax/conventions.md)**

## Contribute

If you want to contribute, join our [Discord](https://discord.gg/6eKf5ZY).

## License

This project is licensed under [GFDL-1.3-or-later](https://www.gnu.org/licenses/fdl-1.3.html),
[MIT license](https://choosealicense.com/licenses/mit/) and/or
[CC-BY-SA-3.0 (Creative Commons Attribution-ShareAlike 3.0 Unported license)](https://creativecommons.org/licenses/by-sa/3.0/)

This makes the project both compatible with the
[Dwarf Fortress Wiki](https://dwarffortresswiki.org) and
[Wikipedia](https://www.wikipedia.org/).

All contributions to this project will be similarly licensed.