# Body Tokens

Body tokens determine bodily structure and materials.

Wiki: https://dwarffortresswiki.org/index.php/Body_token

TODO

## Body Tokens
<a name="body_tokens"></a>

```mermaid
graph LR; 
    Start(( )) --> BodyToken --> End(( ));
    BodyToken --> BodyToken;
    click BodyToken "#body_token"
```

## Body Token
<a name="body_token"></a>
Example: `[BODY:BASIC_3PARTARMS] ...`

```mermaid
graph LR; 
    %% OT = ObjectType
    Start(( )) --> OpenBracket(("["));
    OpenBracket --> OTBody([BODY]) --> Colon((":")) --> TokenValueReference --> CloseBracket;
    CloseBracket(("]")) --> BodyBodyTokens --> End(( ));
    BodyBodyTokens --> BodyBodyTokens;
    click TokenValueReference "start.md#token_value_reference";
    click BodyBodyTokens "#body_body_tokens";
```

## Bodygloss Token
<a name="bodygloss_token"></a>
Example: `[BODYGLOSS:MAW:mouth:maw:mouths:maws] ...`

```mermaid
graph LR; 
    %% OT = ObjectType
    Start(( )) --> OpenBracket(("["));
    OpenBracket --> OTBody([BODYGLOSS]) --> Colon((":")) --> TokenValueReference --> Colon2((":")) --> 1["TokenValueString"] --> Colon3((":")) --> 2["TokenValueString"] --> Colon4((":")) --> 3["TokenValueString"] --> Colon5((":")) --> 4["TokenValueString"] --> Close(("]")) --> End((" "))
    click TokenValueReference "start.md#token_value_reference";
    click TokenValueString "start.md#token_value_string";   
```

## Body (Body) Tokens
<a name="body_body_tokens"></a>

```mermaid
graph LR; 
    %% OT = ObjectType
    Start(( ));
    End(( ));
    Start --> Bodypart --> End;
    click Bodypart "#body_bp_token";
```

## Body Bodypart Token
<a name="body_bp_token"></a>
Example: `[BP:RUA:right upper arm:STP] ...`

```mermaid
graph LR; 
    Start(( ));
    End(( ));
    OpenBracket(("["));
    CloseBracket(("]"));
    Start --> OpenBracket;
    OpenBracket --> OTAddBP([BP]) --> Colon((":")) --> BPId["TokenValueReference"] --> Colon2((":")) --> BPStr["TokenValueString"] --> Colon3((":")) --> BPPlural["TokenValueReference"] --> CloseBracket --> BodypartBodyTokens["BodypartBodyTokens"] --> End(( ));
    BodypartBodyTokens --> BodypartBodyTokens;
    BPStr --> CloseBracket;
    click BPId "start.md#token_value_reference"; 
    click BPPlural "start.md#token_value_string";  
    click BPStr "start.md#token_value_reference";           
    click BodypartBodyTokens "bp_body_tokens";  
```

## Bodypart Body Tokens
<a name="bp_body_tokens"></a>

```mermaid
graph LR; 
    %% OT = ObjectType
    Start(( ));
    End(( ));
    Start --> ConType --> End;
    Start --> Category --> End;    
    click ConType "#bp_con_token";
    click Category "#bp_cat_token";    
```

## Bodypart ConType
<a name="bp_con_token"></a>
Example: `[CONTYPE:UPPERBODY] ...`

```mermaid
graph LR; 
    Start(( ));
    End(( ));
    OpenBracket(("["));
    CloseBracket(("]"));
    Start --> OpenBracket;
    OpenBracket --> OTCon([CONTYPE]) --> Colon((":")) --> ConId["TokenValueReference"] --> CloseBracket --> End;
    click ConId "start.md#token_value_reference";              
```

## Bodypart Category
<a name="bp_cat_token"></a>
Example: `[CATEGORY:ARM_UPPER] ...`

```mermaid
graph LR; 
    Start(( ));
    End(( ));
    OpenBracket(("["));
    CloseBracket(("]"));
    Start --> OpenBracket;
    OpenBracket --> OTCon([CATEGORY]) --> Colon((":")) --> ConId["TokenValueReference"] --> CloseBracket --> End;
    click ConId "start.md#token_value_reference";             
```

