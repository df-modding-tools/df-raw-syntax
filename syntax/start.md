# Start of the syntax of DF Raw files

Dwarf Fortress raw file have the `.txt` extension.
The files should be [CP437](https://en.wikipedia.org/wiki/Code_page_437) encoded.

## Start
<a name="start"></a>
Starting point for DF Raw files:
Regex: `^([ \t]*\w+[^\n]*)\n([\s\S]*)$`

```mermaid
graph LR; 
    NewLine(("\n"));
    File_Start[[File Start]] --> End_of_File[[End of File]];
    File_Start --> Header --> NewLine --> Body --> End_of_File;
    NewLine --> End_of_File;
    click Body "#body"
    click Header "#header"
```

## Header
<a name="header"></a>
The header can be any character as long
Regex: `[ \t]*\w+[^\n]*`

```mermaid
graph LR; 
    Start(( )) --> Header["Any characters except `\n`, at least 1 , non whitespace, character"];
    Header --> End(( ));
```

## Body
<a name="body"></a>

```mermaid
graph LR;
    Start(( ));
    End(( ));
    Start --> ObjectToken --> End;
    Start --> Comment --> End;
    End --> Start;
    click ObjectToken "#token"
    click Comment "#comment"
    click Whitespace "#whitespace"
```

## Object Token
<a name="object_token"></a>
You can find a full list of all the tokens here: 
https://dwarffortresswiki.org/index.php/Raw_file#Types_of_content
And more info here: https://dwarffortresswiki.org/index.php/Token

```mermaid
graph LR; 
    %% OB = OpenBracket
    %% CB = CloseBracket
    %% OT = ObjectType
    Start(( )) --> OB(("["));
    Colon((":"));
    Object([OBJECT]);
    End(( ));
    OB --> Object --> Colon;
    Colon--> OTBody([BODY]) -->CB1(("]"))--> BodyTokens -->End;
    Colon--> OTBodyDetail([BODY_DETAIL_PLAN]) -->CB2(("]"))--> BodyDetailTokens -->End;
    Colon--> OTBuilding([BUILDING]) -->CB3(("]"))--> BuildingTokens -->End;
    Colon--> OTCreature([CREATURE]) -->CB4(("]"))--> CreatureTokens -->End;
    Colon--> OTCreatureVar([CREATURE_VARIATION]) -->CB5(("]"))--> CreatureVariationTokens -->End;
    Colon--> OTDescriptorColor([DESCRIPTOR_COLOR]) -->CB6(("]"))--> DescriptorColorTokens -->End;
    Colon--> OTDescriptorPattern([DESCRIPTOR_PATTERN]) -->CB7(("]"))--> DescriptorPatternTokens -->End;
    Colon--> OTDescriptorShape([DESCRIPTOR_SHAPE]) -->CB8(("]"))--> DescriptorShapeTokens -->End;
    Colon--> OTEntity([ENTITY]) -->CB9(("]"))--> EntityTokens -->End;
    Colon--> OTGraphics([GRAPHICS]) -->CB10(("]"))--> GraphicsTokens -->End;
    Colon--> OTInteraction([INTERACTION]) -->CB11(("]"))--> InteractionTokens -->End;
    Colon--> OTInorganic([INORGANIC]) -->CB12(("]"))--> InorganicTokens -->End;
    Colon--> OTItem([ITEM]) -->CB13(("]"))--> ItemTokens -->End;
    Colon--> OTLanguage([LANGUAGE]) -->CB14(("]"))--> LanguageTokens -->End;
    Colon--> OTMaterial([MATERIAL_TEMPLATE]) -->CB15(("]"))--> MaterialTokens -->End;
    Colon--> OTPlant([PLANT]) -->CB16(("]"))--> PlantTokens -->End;
    Colon--> OTReaction([REACTION]) -->CB17(("]"))--> ReactionTokens -->End;
    Colon--> OTTissue([TISSUE_TEMPLATE]) -->CB18(("]"))--> TissueTokens -->End;
    click BodyTokens "body_tokens.md"
    click BodyDetailTokens "body_detail_tokens.md"
    click BuildingTokens "building_tokens.md"
    click CreatureTokens "creature_tokens.md"
    click CreatureVariationTokens "creature_variation_tokens.md"
    click DescriptorColorTokens "descriptor_color_tokens.md"
    click DescriptorPatternTokens "descriptor_pattern_tokens.md"
    click DescriptorShapeTokens "descriptor_shape_tokens.md"
    click EntityTokens "entity_tokens.md"
    click GraphicsTokens "graphics_tokens.md"
    click InteractionTokens "interaction_tokens.md"
    click InorganicTokens "inorganic_tokens.md"
    click ItemTokens "item_tokens.md"
    click LanguageTokens "language_tokens.md"
    click MaterialTokens "material_tokens.md"
    click PlantTokens "plant_tokens.md"
    click ReactionTokens "reaction_tokens.md"
    click TissueTokens "tissue_tokens.md"
```

## Token
<a name="token"></a>
This is the general layout of a token.

```mermaid
graph LR; 
    Start(( ));
    End(( ));
    OpenBracket(("["));
    CloseBracket(("]"));
    Colon((":"));
    Start --> OpenBracket;
    OpenBracket --> TokenValueReference --> Colon --> TokenValues --> CloseBracket;
    CloseBracket --> End;
    TokenValueReference --> CloseBracket;
    click TokenValueReference "#token_value_reference"
    click TokenValues "#token_values"
```

## Token Values
<a name="token_values"></a>
This is the general layout of a token values.

```mermaid
graph LR;
    Start(( ));
    End(( ));
    Start --> TokenValueNumber --> End;
    Start --> TokenValueCharacter--> End;
    Start --> TokenValueString--> End;
    Start --> TokenValueReference--> End;
    End --> Colon((":")) --> Start;
    click TokenValueNumber "#token_value_number"
    click TokenValueCharacter "#token_value_character"
    click TokenValueString "#token_value_string"
    click TokenValueReference "#token_value_reference"
```

## Token Value Number
<a name="token_value_number"></a>
Regex: `-?[0-9]+` or `\d+`

```mermaid
graph LR;
    Start(( )) --> TVNumber["A number: `-?[0-9]+` "] --> End(( ));
```

## Token Value Character
<a name="token_value_character"></a>
Regex: `'.'` or `[0-9]{1,3}`

```mermaid
graph LR;
    Start(( )) --> OpenQuote(("'")) --> TVCharQuotes["One CP437 character"] --> CloseQuote(("'")) --> End(( ));
    Start(( )) --> TVIndex["An index: `[0-9]{1,3}`"] --> End(( ));
```

## Token Value String
<a name="token_value_string"></a>
Regex: `[^\[\]:\n\r]+`

```mermaid
graph LR;
    Start(( )) --> TVString["Any character, except `[`, `]` or `:` "] --> End(( ));
```

## Token Value Reference
<a name="token_value_reference"></a>
Regex: `[0-9]*[A-Z][A-Z_0-9]*`

```mermaid
graph LR;
    Start(( )) --> TVReference["An reference to an accepted label, all uppercase"] --> End(( ));
```

## Comment
<a name="comment"></a>
Basically everything that is not in the `[...]` is considered a comment except for the header (first line).

In further documentation of the syntax we leave out the comments for clarity.
But comments can be present everywhere outside of the `[...]` tokens.

```mermaid
graph LR;
    Start(( ));
    End(( ));
    Start --> End;
    Start --> Comment["Any character, except `[` or `]` "] --> End;
    Start --> Whitespace --> End;
```

## Whitespace
<a name="whitespace"></a>
Regex: `\s*`

In further documentation of the syntax we leave out the whitespace for clarity.
But whitespace can be present everywhere outside of the `[...]` tokens.

```mermaid
graph LR;
    Start(( ));
    End(( ));
    Start --> End;
    Start --> Space["Space: ' '"] --> End;
    Start --> Linefeed["Linefeed: '\n'"] --> End;
    Start --> CarriageReturn["Carriage return: '\r'"] --> End;
    Start --> Tab["Horizontal tab: '\t'"] --> End;
    End --> Start;
```