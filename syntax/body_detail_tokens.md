# Body Detail (Plan) Tokens

Body detail plan tokens define some details of a body, similar to body tokens.

Wiki: https://dwarffortresswiki.org/index.php/Body_detail_plan_token

TODO

## Body Detail (Plan) Tokens
<a name="bdp_tokens"></a>

```mermaid
graph LR; 
    Start(( )) --> BodyDetailToken --> End(( ));
    BodyDetailToken --> BodyDetailToken;
    click BodyDetailToken "#bdp_token"
```

## Body Detail (Plan) Token
<a name="bdp_token"></a>
Example: `[BODY_DETAIL_PLAN:STANDARD_MATERIALS] ...`

```mermaid
graph LR; 
    %% OT = ObjectType
    Start(( )) --> OpenBracket(("["));
    OpenBracket --> OTBodyDetail([BODY_DETAIL_PLAN]) --> Colon((":")) --> TokenValueReference --> CloseBracket;
    CloseBracket(("]")) --> BodyDetailBodyTokens --> End(( ));
    BodyDetailBodyTokens --> BodyDetailBodyTokens;
    click TokenValueReference "start.md#token_value_reference";
    click BodyDetailBodyTokens "#bdp_body_tokens";
```

## Body Detail (Plan) Body Tokens
<a name="bdp_body_tokens"></a>

```mermaid
graph LR; 
    %% OT = ObjectType
    Start(( ));
    End(( ));
    Start --> AddMaterial --> End;
    click AddMaterial "#bdp_add_token";
```

## Body Detail (Plan) Add Material
<a name="bdp_add_token"></a>
Example: `[ADD_MATERIAL:SKIN:SKIN_TEMPLATE]`

```mermaid
graph LR; 
    Start(( ));
    End(( ));
    OpenBracket(("["));
    CloseBracket(("]"));
    Start --> OpenBracket;
    OpenBracket --> OTAddMat([ADD_MATERIAL]) --> Colon((":")) --> SkinId["TokenValueReference"] --> Colon2((":")) --> SkinTemp["TokenValueReference"] --> CloseBracket;
    CloseBracket --> End;
    click SkinTemp "tissue_tokens.md";   
    click SkinId "start.md#token_value_reference";    
```


