# Entity Tokens

Entity tokens define entities, or [civilizations](https://dwarffortresswiki.org/index.php/Civilization).

Wiki: https://dwarffortresswiki.org/index.php/Entity_token

TODO

## Entity Tokens
<a name="entity_tokens"></a>

```mermaid
graph LR; 
    Start(( )) --> EntityToken --> End(( ));
    EntityToken --> EntityToken;
    click EntityToken "#entity_token"
```

## Entity Token
<a name="entity_token"></a>
Example: `[ENTITY:MOUNTAIN] ...`

```mermaid
graph LR; 
    %% OT = ObjectType
    Start(( )) --> OpenBracket(("["));
    OpenBracket --> OTEntity([ENTITY]) --> Colon((":")) --> TokenValueReference --> CloseBracket;
    CloseBracket(("]")) --> EntityBodyTokens --> End(( ));
    EntityBodyTokens --> EntityBodyTokens;
    click TokenValueReference "start.md#token_value_reference"
    click EntityBodyTokens "#entity_body_tokens"
```

## Entity Body Tokens
<a name="entity_body_tokens"></a>

```mermaid
graph LR; 
    %% OT = ObjectType
    Start(( ));
    End(( ));
    Start --> SiteControllable --> End;
    Start --> AllMainPopsControllable --> End;
    Start --> Creature --> End;
    Start --> Currency --> End;
    click SiteControllable "#entity_site_controllable_token"
    click AllMainPopsControllable "#entity_all_main_pops_controllable_token"
    click Creature "#entity_creature_token"
    click Currency "#entity_currency_token"
```

## Entity Site Controllable
<a name="entity_site_controllable_token"></a>
Example: `[SITE_CONTROLLABLE]`

```mermaid
graph LR; 
    Start(( ));
    End(( ));
    OpenBracket(("["));
    CloseBracket(("]"));
    Start --> OpenBracket;
    OpenBracket --> OTSiteControllable([SITE_CONTROLLABLE]) --> CloseBracket;
    CloseBracket --> End;
```

## Entity All Main Pops Controllable
<a name="entity_all_main_pops_controllable_token"></a>
Example: `[ALL_MAIN_POPS_CONTROLLABLE]`

```mermaid
graph LR; 
    Start(( ));
    End(( ));
    OpenBracket(("["));
    CloseBracket(("]"));
    Start --> OpenBracket;
    OpenBracket --> OTAllMainPopControllable([ALL_MAIN_POPS_CONTROLLABLE]) --> CloseBracket;
    CloseBracket --> End;
```

## Entity Creature
<a name="entity_creature_token"></a>
Example: `[CREATURE:DWARF]`

```mermaid
graph LR; 
    Start(( ));
    End(( ));
    OpenBracket(("["));
    CloseBracket(("]"));
    Colon((":"));
    Start --> OpenBracket;
    OpenBracket --> OTCreature([CREATURE]) --> Colon --> TokenValueReference --> CloseBracket;
    CloseBracket --> End;
    click TokenValueReference "start.md#token_value_reference"
```

## Entity Currency
<a name="entity_currency_token"></a>
Example: `[CURRENCY:COPPER:1]`

```mermaid
graph LR; 
    Start(( ));
    End(( ));
    OpenBracket(("["));
    CloseBracket(("]"));
    Colon((":"));
    Start --> OpenBracket;
    OpenBracket --> OTCreature([CURRENCY])
        --> Colon --> TokenValueReference
        --> Colon2((:)) --> TokenValueNumber
        --> CloseBracket;
    CloseBracket --> End;
    click TokenValueReference "start.md#token_value_reference"
    click TokenValueNumber "start.md#token_value_number"
```
