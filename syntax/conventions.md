# Conventions in Dwarf Fortress RAW

This syntax assumes you follow the conventions in this document. This is to ensure all DF RAW files
look similar and will work correctly and doesn't have unintended behavior.

These conventions are created my the community while creating the syntax.
This is also to prevent people form arguing about things. The conventions are based on the RAW
files created by [Tarn](https://en.wikipedia.org/wiki/Tarn_Adams) that are part of vanilla DF.

## Naming convention

In the syntax you have roughly 6 different types:
- Integers: The are a set of whole number. Allowed characters are `0` to `9`.
    For negative number a leading `-` is added.
    - Examples: `-5`, `8`, `25`, `156`.
    - Regex: `-?[0-9]+`
- Tokens: A token is the first item after the open `[` for example `[OBJECT:...]`
    the token is `OBJECT`. These follow the `SCREAMING_SNAKE_CASE` convention.
    The can have numbers in them, but must at least have 1 Uppercase character.
    The `_` character are allowed.
    - Examples: `OUTDOOR_WOOD`, `SOLID_DENSITY`, `NAME`
    - Regex: `[0-9]*[A-Z][A-Z_0-9]*`
- References/IDs: These are names given to values that will used internally
    by the game. They use the same convention as Tokens.
    - Examples: `GENERAL`, `DWARF`, `ITEM_WEAPON_HALBERD`, `2EYES`
    - Regex: `[0-9]*[A-Z][A-Z_0-9]*`
- Enums: These are values that have a limited set of accepted values.
    These values are hardcoded by the game.
    They also follow the same conventions as Token and IDs.
    - Examples: `COMMON`, `UNTHINKABLE`, `PLURAL`
    - Regex: `[0-9]*[A-Z][A-Z_0-9]*`
- Character: A character used to print on the screen. Because DF uses
    [CP437](https://en.wikipedia.org/wiki/Code_page_437) they are sometimes referred 
    to as there index on the code page/table. Accepted are any (one) CP437 character 
    (correctly encoded in the file) in between single quotes. 
    For example: `'a'`, `'G'`, `'''`.
    An index can also be used. The number must be between 0 and 255 (inclusive).
    - Examples: `5`,`'p'`, `'{'`, `206`
    - Regex: `('.')|([0-9]{1,3})`
- Strings: A piece of text, a string of characters. They can be any CP437 character.
    Any character is allowed excepts for `[`, `]`, `:`, `\n` and `\r`.
    As those are reserved for the token handling.
    The string must be at least 1 character long.
    The empty string is permitted by the game, but never encourages to be used.
    If a string is all uppercase the syntax highlighter might mark it as an ID.
    This can be prevented by adding a space(` `).
    - Example: `this is a string`, `This, 3 { is } TOO.`, `0sk#osp`
    - Regex: `[^\[\]:\n\r]+`
