# Descriptor Color Tokens

Descriptor color tokens define colors used in text-based descriptions.
Not to be confused with [color schemes](https://dwarffortresswiki.org/index.php/Color_scheme).

Wiki: https://dwarffortresswiki.org/index.php/Descriptor_color_token

TODO